module check::CheckVarsTyped
import syn::SimplVarsTyped;
import eval::Environment;
import String;
import ParseTree;
import IO;




@doc{Types. Our types are bools and integers.}
data Type = Int() | Bool();

public default Type checkExpr(Expr expr, Env[Type] env) {
	throw "Unknown expr: <[expr]>";
}

@doc{The type of an integer literal is int.}
public Type checkExpr((Expr)`<INT i>`, Env[Type] env) {
	return Int();
}

@doc{The type of an boolean literal is bool.}
public Type checkExpr((Expr)`<Bool b>`, Env[Type] env) {
	return Bool();
}

@doc{The value of a variable is the value it's bound to in the environment.}
public Type checkExpr((Expr)`<ID x>`, Env[Type] env) {
	if(isDefined(x, env))
		return lookup(x, env);
	else
		throw "Undefined variable: <x>";
}

public Type checkExpr((Expr)`(<Expr e>)`, Env[Type] env) {
	return checkExpr(e, env);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Type checkExpr((Expr)`<Expr e1>*<Expr e2>`, Env[Type] env) {
	t1 = checkExpr(e1, env);
	t2 = checkExpr(e2, env);
	
	if(t1 == Int() && t2 == Int())
		return Int();
	else
		throw "Type error: expected (int, int), got (<t1>, <t2>)"; 
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Type checkExpr((Expr)`<Expr e1>+<Expr e2>`, Env[Type] env) {
	t1 = checkExpr(e1, env);
	t2 = checkExpr(e2, env);
	
	if(t1 == Int() && t2 == Int())
		return Int();
	else
		throw "Type error: expected (int, int), got (<t1>, <t2>)"; 
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Type checkExpr((Expr)`<Expr e1>-<Expr e2>`, Env[Type] env) {
	t1 = checkExpr(e1, env);
	t2 = checkExpr(e2, env);
	
	if(t1 == Int() && t2 == Int())
		return Int();
	else
		throw "Type error: expected (int, int), got (<t1>, <t2>)"; 
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Type checkExpr((Expr)`<Expr e1>/<Expr e2>`, Env[Type] env) {
	t1 = checkExpr(e1, env);
	t2 = checkExpr(e2, env);
	
	if(t1 == Int() && t2 == Int())
		return Int();
	else
		throw "Type error: expected (int, int), got (<t1>, <t2>)"; 
}

@doc{Comparison. Evaluate the operands, then apply the comparison operator.}
public Type checkExpr((Expr)`<Expr e1>==<Expr e2>`, Env[Type] env) {
	t1 = checkExpr(e1, env);
	t2 = checkExpr(e2, env);
	
	if(t1 == t2)
		return Bool();
	else
		throw "Type error: operands have different types (<t1>, <t2>)"; 
}

@doc{Comparison. Evaluate the operands, then apply the comparison operator.}
public Type checkExpr((Expr)`<Expr e1>\<<Expr e2>`, Env[Type] env) {
	t1 = checkExpr(e1, env);
	t2 = checkExpr(e2, env);
	
	if(t1 == Int() && t2 == Int())
		return Bool();
	else
		throw "Type error: expected (int, int), got (<t1>, <t2>)"; 
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public Type checkExpr((Expr)
       `if <Expr c> then <Expr e1> else <Expr e2> end`, Env[Type] env) {
    if(checkExpr(c, env) != Bool())
    	throw "Condition must be bool";
    
	t1 = checkExpr(e1, env);
	t2 = checkExpr(e2, env);
	
	if(t1 == t2)
		return t1;
	else
		throw "Type error: branches have different types (<t1>, <t2>)"; 
}

@doc{'Let' expression. Evaluate the first expression, then evaluate the
second in an environment where the variable is bound to the result of the
first expression.}
public Type checkExpr((Expr)
       `let <TypeExpr te> <ID x> = <Expr e1> in <Expr e2> end`,
       Env[Type] env) {
	typ = checkType(te, env);
	t1 = checkExpr(e1, env);
	if(typ != t1)
		throw "Type mismatch declaring <x>: expected <typ>, got <t1>";
		
	return checkExpr(e2, declare(x, typ, env));
}

public Type checkType((TypeExpr)`int`, Env[Type] env) {
	return Int();
}

public Type checkType((TypeExpr)`bool`, Env[Type] env) {
	return Bool();
}

public Type checkExpr((Program)`<Expr e>`) {
	return checkExpr(e, newEnv(#Type));
}

test bool Numbers() { return checkExpr((Program)`5`) == Int(); }

test bool Arith1() { return checkExpr((Program)`5+5`) == Int(); }
test bool Arith2() { return checkExpr((Program)`1+2*3`) == Int(); }
test bool Arith3() { return checkExpr((Program)`10-2`) == Int(); }
test bool Arith4() { return checkExpr((Program)`10/2`) == Int(); }
test bool Arith5() { return checkExpr((Program)`(1+2)*3`) == Int(); }
test bool Arith6() { return checkExpr((Program)`((10)/(2))`) == Int(); }

test bool If1() { return checkExpr((Program)`if 2*3 == 6 then 42 else 69 end`) == Int(); }
test bool If2() { return checkExpr((Program)`if 6 == 5 then 42 else 69 end`) == Int(); }
test bool If3() { return checkExpr((Program)`if 10 \< 5 then 1 else 0 end`) == Int(); }

module eval::EvalLex
import syn::SimplFuns;
import String;
import ParseTree;
import IO;
import eval::Environment;
import List;

@doc{Values. Integers and functions are values}
data Value
	= Int(int intValue)
	| Fun(list[ID] params, Expr body, Env[Value] env)
	;

public str toString(Int(i)) {
	return "<i>";
}

public str toString(Fun(params, body)) {
	return "Fun([<intercalate(", ", ["(ID)`<unparse(p)>`" | p <- params])>], (Expr)`<unparse(body)>`)";
}

public void printlnValue(Value v) {
	println(toString(v));
}

@doc{The value of an integer literal is the value of integer itself.}
Value eval((Expr)`<INT i>`, Env[Value] env) {
	return Int(toInt(unparse(i)));
}

@doc{The value of a variable is the value it's bound to in the environment.}
Value eval((Expr)`<ID x>`, Env[Value] env) {
	return lookup(x, env);
}

@doc{The value of a function is itself, as a value.}
Value eval((Expr)`fun <{ID ","}* params> =\> <Expr body>`,
  Env[Value] env) {
	return Fun([p | p <- params], body, env);
}
/*
Value eval((Expr)`<Expr f>(<{Expr ","}* args>)`, Env[Value] env) {
	fVal = eval(f, env); // evaluate function expression
	if(fVal is Fun) {
		fEnv = fVal.env; // function environment
		for(<arg, param> <- zip([a | a <- args], fVal.params)) {
			argVal = eval(arg, env); // evaluate argument
			fEnv = declare(param, argVal, fEnv); // bind parameter
		}
	
		return eval(fVal.body, fEnv); // evaluate body in fEnv
	}
	else throw "Calling a non-function: <unparse(f)>, at <f@\loc>";
}
*/

Value eval((Expr)`<Expr f>(<{Expr ","}* args>)`, Env[Value] env) {
	fVal = eval(f, env); // evaluate function expression
	if(fVal is Fun) {
		fEnv = fVal.env; // function environment
		if((Expr)`<ID fName>` := f) { // check if f is a name
			fEnv = declare(fName, fVal, fEnv); // for recursive calls
		}
		for(<arg, param> <- zip([a | a <- args], fVal.params)) {
			argVal = eval(arg, env); // evaluate argument
			fEnv = declare(param, argVal, fEnv); // bind parameter
		}
	
		return eval(fVal.body, fEnv); // evaluate body in fEnv
	}
	else throw "Calling a non-function: <unparse(f)>, at <f@\loc>";
}

Value eval((Expr)`(<Expr e>)`, Env[Value] env) {
	return eval(e, env);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
Value eval((Expr)`<Expr e1>*<Expr e2>`, Env[Value] env) {
	return Int(eval(e1, env).intValue * eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
Value eval((Expr)`<Expr e1>+<Expr e2>`, Env[Value] env) {
	return Int(eval(e1, env).intValue + eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
Value eval((Expr)`<Expr e1>-<Expr e2>`, Env[Value] env) {
	return Int(eval(e1, env).intValue - eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
Value eval((Expr)`<Expr e1>/<Expr e2>`, Env[Value] env) {
	return Int(eval(e1, env).intValue / eval(e2, env).intValue);
}

@doc{Comparison. Evaluate the operands, then apply the comparison operator.}
Value eval((Expr)`<Expr e1>==<Expr e2>`, Env[Value] env) {
	return Int(eval(e1, env).intValue == eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, then apply the comparison operator.}
Value eval((Expr)`<Expr e1>\<<Expr e2>`, Env[Value] env) {
	return Int(eval(e1, env).intValue < eval(e2, env).intValue ? 1 : 0);
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
Value eval((Expr)
       `if <Expr c> then <Expr e1> else <Expr e2> end`, Env[Value] env) {
	if(eval(c, env).intValue != 0)
		return eval(e1, env);
	else
		return eval(e2, env);
}

@doc{'Let' expression. Evaluate the first expression, then evaluate the
second in an environment where the variable is bound to the result of the
first expression.}
Value eval((Expr)
     `let <ID x> = <Expr e1> in <Expr e2> end`, Env[Value] env) {
	v = eval(e1, env);
	return eval(e2, declare(x, v, env));
}

public Value eval((Program)`<Expr e>`) {
	return eval(e, newEnv(#Value));
}
public default Value eval(Tree t) {
	if(t has top)
		return eval(t.top);
	else
		throw "no evaluator for <[t]>";
}

public default Value eval(Tree t, Env) {
	throw "no evaluator for <[t]>";
}
test bool Numbers() { return eval((Program)`5`) == Int(5); }

test bool Arith1() { return eval((Program)`5+5`) == Int(10); }
test bool Arith2() { return eval((Program)`1+2*3`) == Int(7); }
test bool Arith3() { return eval((Program)`10-2`) == Int(8); }
test bool Arith4() { return eval((Program)`10/2`) == Int(5); }
test bool Arith5() { return eval((Program)`(1+2)*3`) == Int(9); }
test bool Arith6() { return eval((Program)`((10)/(2))`) == Int(5); }

test bool If1() { return eval((Program)`if 2*3 == 6 then 42 else 69 end`) == Int(42); }
test bool If2() { return eval((Program)`if 6 == 5 then 42 else 69 end`) == Int(69); }
test bool If3() { return eval((Program)`if 10 \< 5 then 1 else 0 end`) == Int(0); }

module eval::EvalTrivial
import syn::SimplTrivial;
import String;
import ParseTree;
import IO;

@doc{Values. Integers and functions are values}
data Value = Int(int intValue);

@doc{The value of an integer literal is the value of integer itself.}
public Value eval((Expr)`<INT i>`) {
	return Int(toInt(unparse(i)));
}

public Value eval((Expr)`(<Expr e>)`) {
	return eval(e);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>*<Expr e2>`) {
	return Int(eval(e1).intValue * eval(e2).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>+<Expr e2>`) {
	return Int(eval(e1).intValue + eval(e2).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>-<Expr e2>`) {
	return Int(eval(e1).intValue - eval(e2).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>/<Expr e2>`) {
	return Int(eval(e1).intValue / eval(e2).intValue);
}

@doc{Comparison. Evaluate the operands, then apply the comparison operator.}
public Value eval((Expr)`<Expr e1>==<Expr e2>`) {
	return Int(eval(e1).intValue == eval(e2).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, then apply the comparison operator.}
public Value eval((Expr)`<Expr e1>\<<Expr e2>`) {
	return Int(eval(e1).intValue < eval(e2).intValue ? 1 : 0);
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public Value eval((Expr)
       `if <Expr c> then <Expr e1> else <Expr e2> end`) {
	if(eval(c).intValue != 0)
		return eval(e1);
	else
		return eval(e2);
}

public Value eval((Program)`<Expr e>`) {
	return eval(e);
}

test bool Numbers() { return eval((Program)`5`) == Int(5); }

test bool Arith1() { return eval((Program)`5+5`) == Int(10); }
test bool Arith2() { return eval((Program)`1+2*3`) == Int(7); }
test bool Arith3() { return eval((Program)`10-2`) == Int(8); }
test bool Arith4() { return eval((Program)`10/2`) == Int(5); }
test bool Arith5() { return eval((Program)`(1+2)*3`) == Int(9); }
test bool Arith6() { return eval((Program)`((10)/(2))`) == Int(5); }

test bool If1() { return eval((Program)`if 2*3 == 6 then 42 else 69 fi`) == Int(42); }
test bool If2() { return eval((Program)`if 6 == 5 then 42 else 69 fi`) == Int(69); }
test bool If3() { return eval((Program)`if 10 \< 5 then 1 else 0 fi`) == Int(0); }

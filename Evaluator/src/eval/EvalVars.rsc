module eval::EvalVars
import syn::SimplVars;
import String;
import ParseTree;
import IO;

@doc{The type of environments; maps from strings to values}
alias Env = map[str, Value];

@doc{Values. Integers and functions are values}
data Value = Int(int intValue);

@doc{The value of an integer literal is the value of integer itself.}
public Value eval((Expr)`<INT i>`, Env env) {
	return Int(toInt(unparse(i)));
}

@doc{The value of a variable is the value it's bound to in the environment.}
public Value eval((Expr)`<ID x>`, Env env) {
	return env[unparse(x)];
}

public Value eval((Expr)`(<Expr e>)`, Env env) {
	return eval(e, env);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>*<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue * eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>+<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue + eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>-<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue - eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>/<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue / eval(e2, env).intValue);
}

@doc{Comparison. Evaluate the operands, then apply the comparison operator.}
public Value eval((Expr)`<Expr e1>==<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue == eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, then apply the comparison operator.}
public Value eval((Expr)`<Expr e1>\<<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue < eval(e2, env).intValue ? 1 : 0);
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public Value eval((Expr)
       `if <Expr c> then <Expr e1> else <Expr e2> end`, Env env) {
	if(eval(c, env).intValue != 0)
		return eval(e1, env);
	else
		return eval(e2, env);
}

@doc{'Let' expression. Evaluate the first expression, then evaluate the
second in an environment where the variable is bound to the result of the
first expression.}
public Value eval((Expr)
       `let <ID x> = <Expr e1> in <Expr e2> end`, Env env) {
	v = eval(e1, env);
	return eval(e2, env + (unparse(x) : v));
}

public Value eval((Program)`<Expr e>`) {
	return eval(e, ());
}

test bool Numbers() { return eval((Program)`5`) == Int(5); }

test bool Arith1() { return eval((Program)`5+5`) == Int(10); }
test bool Arith2() { return eval((Program)`1+2*3`) == Int(7); }
test bool Arith3() { return eval((Program)`10-2`) == Int(8); }
test bool Arith4() { return eval((Program)`10/2`) == Int(5); }
test bool Arith5() { return eval((Program)`(1+2)*3`) == Int(9); }
test bool Arith6() { return eval((Program)`((10)/(2))`) == Int(5); }

test bool If1() { return eval((Program)`if 2*3 == 6 then 42 else 69 fi`) == Int(42); }
test bool If2() { return eval((Program)`if 6 == 5 then 42 else 69 fi`) == Int(69); }
test bool If3() { return eval((Program)`if 10 \< 5 then 1 else 0 fi`) == Int(0); }

module syn::SimplFunsTyped
extend syn::SimplLexical;

start syntax Program = Expr expr;

syntax Expr
	= Name var
	| INT intVal
	| Bool boolVal
	| bracket "(" Expr e ")"
	| Expr fun "(" {Expr ","}* args ")"
	> left (Expr e1 "*" Expr e2 | Expr e1 "/" Expr e2)
	> left (Expr e1 "+" Expr e2 | Expr e1 "-" Expr e2)
	> left (Expr e1 "==" Expr e2 | Expr e1 "\<" Expr e2)
	| "if" Expr cond "then" e1 Expr "else" Expr e2 "end"
	| "let" TypeExpr Name var "=" Expr e1 "in" Expr e2 "end"
	> "fun" {Param ","}* params "=\>" Expr body 
	;

syntax Param
	= TypeExpr paramType ID paramName
	;
	
syntax Name = ID \ Bool;

syntax Bool = "true" | "false";

syntax TypeExpr
	= "int"
	| "bool"
	| TypeExpr "(" {TypeExpr ","}* ")"
	;

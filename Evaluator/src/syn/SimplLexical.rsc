module syn::SimplLexical
	
// identifiers
//    y !<< x means 'x' must not be preceeded by  'y'
//    x !>> y means 'x' must not by followed by 'y'
// so, this means that an identifier is a sequence of one
// or more letters or underscores, with no additional
// letters or underscores before or after
lexical ID = [a-zA-Z0-9_] !<< [a-zA-Z_] [a-zA-Z_0-9]* !>> [a-zA-Z0-9_];

// numbers
lexical INT = [0-9] !<< [0-9]+ !>> [0-9];

layout LAYOUT 
	= WSorCOM*
	  !>> [\u0009-\u000D \u0020 \u0085 \u00A0 \u1680 \u180E \u2000-\u200A \u2028 \u2029 \u202F \u205F \u3000]
	  !>> "//";
  
lexical WSorCOM 
	= WS | COM | ANNO
	; 

lexical COM
	= @category="Comment" "//" ![\n\r]* $
	;

lexical WS 
	= [\u0009-\u000D \u0020 \u0085 \u00A0 \u1680 \u180E \u2000-\u200A \u2028 \u2029 \u202F \u205F \u3000]
	; 

lexical ANNO
	= [\[] [.:|]
	| [.:|] [\]]
	| "[#" (![#] | [#] !>> [\]])* "#]" 
	;

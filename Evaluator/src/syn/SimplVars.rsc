module syn::SimplVars
extend syn::SimplLexical;

start syntax Program = Expr expr;

syntax Expr
	= ID var
	| INT num
	| bracket "(" Expr e ")"
	> left (Expr e1 "*" Expr e2 | Expr e1 "/" Expr e2)
	> left (Expr e1 "+" Expr e2 | Expr e1 "-" Expr e2)
	> left (Expr e1 "==" Expr e2 | Expr e1 "\<" Expr e2)
	| "if" Expr cond "then" e1 Expr "else" Expr e2 "end"
	| "let" ID var "=" Expr e1 "in" Expr e2 "end" 
	;

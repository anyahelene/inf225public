module util::Tree2LaTeX
import Node;
import List;
import String;
import ParseTree;

alias Config = map[str,str];

public Config defaultConfig = (
		"extraStyle" : "",
		"interiorStyle" : "interior",
		"leafStyle" : "leaf",
		"listStyle" : "list",
		"rootStyle" : "root",
		"stringStyle" : "string",
		"intStyle" : "int",
		"ambStyle" : "amb",
		"lexStyle" : "lex",
		"litStyle" : "lit",
		"cfStyle" : "cf",
		"layoutStyle" : "layout",
		"nonTerminalStyle" : "nonTerminal",
		"terminalStyle" : "terminal",
		"ambNodeText" : "amb",
		"listNodeText" : "[]",
		"dropLayout" : "true",
		"implodeLex" : "false",
		"quoteLex" : "true"
	);

public str astToLatex(value v, str style) {
	return "\\<astToLatex(v, style, [])>;";
}

public str treeToLatex(Tree t, Config conf) {
	return "\\<treeToLatex(t, conf, [])>;";
}

public str treeToLatex(Tree t) {
	return "\\<treeToLatex(t, defaultConfig, [])>;";
}

str astToLatex(node n, str style, list[int] path) {
	spc = spaces(size(path));
	result = "<spc>node[style=interior,<path == [] ? "style=root," : ""><style>] (<pathToStr(path)>) {<quoteLatex(getName(n))>}\n";
	int i = 1;
	for(c <- getChildren(n)) {
		result += "<spc>  child {\n";
		result += astToLatex(c, style, path + [i]);
		result += "<spc>  }\n";
		i = i + 1;
	}
	return result;
}

str astToLatex(list[&T] l,  str style, list[int] path) {
	spc = spaces(size(path));
	result = "<spc> node[style=list,<path == [] ? "style=root," : ""><style>] (<pathToStr(path)>) {}\n";
	int i = 1;
	for(c <- l) {
		result += "<spc>  child {\n";
		result += astToLatex(c, style, path + [i]);
		result += "<spc>  }\n";
		i = i + 1;
	}
	return result;
}


str astToLatex(str s, str style, list[int] path) {
	spc = spaces(size(path));
	return "<spc> node[style=leaf,style=string,<style>] (<pathToStr(path)>) {<quoteLatex(s)>}\n";
}

str astToLatex(int i, str style, list[int] path) {
	spc = spaces(size(path));
	return "<spc> node[style=leaf,style=int,<style>] (<pathToStr(path)>) {<i>}\n";
}

str astToLatex(value v, str style, list[int] path) {
	spc = spaces(size(path));
	return "<spc> node[style=leaf,style=value,<style>] (<pathToStr(path)>) {<quoteLatex(v)>}\n";
}



str treeToLatex(tree:appl(prod, args), Config conf, list[int] path) {
	spc = spaces(size(path));
	name = symName(prod.def, conf);
	if(name == "")
		return "";
	kindStyle =	kindOf(prod.def);
	result = "<spc>node[<getStyle(conf, path, kindStyle, "nonTerminalStyle", "interiorStyle")>] (<pathToStr(path)>) {<name>}\n";

	if(kindStyle == "layoutStyle" && getText(conf, "dropLayout") == "true") {
		return "";
	}
	else if(kindStyle == "lexStyle" && getText(conf, "implodeLex") == "true") {
		txt = quoteLatex(unparse(tree));
		if(getText(conf, "quoteLex") == "true")
			txt = "\"<txt>\""; 			
		return result + "<spc>child { node[<getStyle(conf, path, kindStyle, "terminalStyle", "leafStyle")>] (<pathToStr([*path, 1])>) {<txt>} }\n";
	}
	else if(kindStyle == "litStyle") {
		return result;
	}
	else {		 
		return result + childrenToLatex(args, conf, path);
	}
}

str treeToLatex(c:char(_), Config conf, list[int] path) {
	spc = spaces(size(path));
	txt = quoteLatex(unparse(c));
	if(getText(conf, "quoteLex") == "true")
		txt = "\"<txt>\""; 			
	result = "<spc>node[<getStyle(conf, path, "terminalStyle", "lexStyle", "leafStyle")>] (<pathToStr(path)>) {<txt>}\n";
	return result;
}

str treeToLatex(amb(alts), Config conf, list[int] path) {
	spc = spaces(size(path));
	result = "<spc>node[<getStyle(conf, path, "interiorStyle", "ambStyle")>] (<pathToStr(path)>) {<getText(conf, "ambNodeText")>}\n";
	result = result + childrenToLatex(alts, conf, path);
	return result;
}

str childrenToLatex(list[Tree] trees, Config conf, list[int] path) {
	spc = spaces(size(path));
	int i = 1;
	result = "";
	for(c <- trees) {
		child = treeToLatex(c, conf, path + [i]);
		if(child != "") {
			result += "<spc>child {\n";
			result += child; 
			result += "<spc>}\n";
			i = i + 1;
		}
	}
	return result;
}

str symName(Symbol def, Config conf) {
	switch(def) {
		case \start(s): return symName(s, conf);
		case \sort(n): return quoteLatex(n);
		case \lex(n): return quoteLatex(n);
		case \layouts(n): return getText(conf, "dropLayout") == "true" ? "" : quoteLatex(n);
		case \keywords(n): return quoteLatex(n);
		case \parameterized-sort(n, ps):
			return "<quoteLatex(n)>[<intercalate(", ", [x | x <- [symName(s, conf) | s <- ps], x != ""])>]";
		case \parameterized-lex(n, ps):
			return "<quoteLatex(n)>[<intercalate(", ", [symName(s, conf) | s <- ps, s != ""])>]";
		case \empty(): return "\\ensuremath{\\epsilon}";
		case \opt(sym): return "<symName(sym, conf)>?";
		case \iter(sym): return "<symName(sym, conf)>+";
		case \iter-star(sym): return "<symName(sym, conf)>*";
		case \iter-seps(sym, seps): {
			xs = [x | x <- [symName(s, conf) | s <- seps], x != ""];
			if(xs != [])
				return "\\{<symName(sym, conf)> <intercalate(" ", xs)>\\}+";
			else
				return "<symName(sym, conf)>+";
		}
		case \iter-star-seps(sym, seps): {
			xs = [x | x <- [symName(s, conf) | s <- seps], x != ""];
			if(xs != [])
				return "\\{<symName(sym, conf)> <intercalate(" ", xs)>\\}*";
			else
				return "<symName(sym, conf)>*";
		}
		case \alt(alts): return "<intercalate(" | ", [x | x <- [symName(s, conf) | s <- alts], x != ""])>";
		case \seq(syms): return "<intercalate(" ", [x | x <- [symName(s, conf) | s <- syms], x != ""])>";
		case \lit(string): return "\"<quoteLatex(string)>\"";
		case \cilit(string): return "\"<quoteLatex(string)>\"";
		case \char-class(string): return quoteLatex(printSymbol(def, false));
		case \conditional(sym, _): return symName(sym, conf);
		default: return "??<def>";
	}
}

str kindOf(Symbol def) {
	switch(def) {
		case \start(s): return kindOf(s);
		case \sort(n): return "cfStyle";
		case \lex(n): return "lexStyle";
		case \layouts(n): return "layoutStyle";
		case \keywords(n): return "litStyle";
		case \parameterized-sort(n, ps): return "cfStyle";
		case \parameterized-lex(n, ps): return "lexStyle";
		case \empty(): return "lexStyle";
		case \opt(sym): return kindOf(sym);
		case \iter(sym): return kindOf(sym);
		case \iter-star(sym): return kindOf(sym);
		case \iter-seps(sym, seps): return kindOf(sym);
		case \iter-star-seps(sym, seps): return kindOf(sym);
		case \alt(alts): return kindOf(getOneOf(alts));
		case \seq(syms): return kindOf(getOneOf(syms));
		case \lit(string): return "litStyle";
		case \cilit(string): return "litStyle";
		case \char-class(string): return "lexStyle";
		case \conditional(sym, _): return kindOf(sym);
		default: return "cfStyle";
	}
}

bool isTerminal(Symbol sym) {
	switch(sym) {
		case \lit(string): return true;
		case \cilit(string): return true;
		case \char-class(string): return true;
		default: return false;
	}
}

str getStyle(Config conf, list[int] path, str ns ...) {
	list[str] styles = [];
	if(path == [])
		ns = [*ns, "rootStyle"];
	ns = [*ns, "extraStyle"];
	for(n <- ns) {
		if(n in conf) {
			s = conf[n];
			if(s != "") {
				styles += ["style=<s>"];
			}
		}
	}
	if(styles != [])
		return intercalate(",", styles);
	else
		return "";
}

str getText(Config conf, str n) {
	if(n in conf)
		return conf[n];
	else
		return "";
}

str spaces(int n) {
	result = "";
	while(n > 0) {
		result += "  ";
		n = n - 1;
	}
	return result;
}

str pathToStr(list[int] path) {
	return "R" + intercalate("-", ["<i>" | i <- path]);
}

str quoteLatex(str s) {
	s = replaceAll(s, "\\", "\\ensuremath\\backslash ");
	s = replaceAll(s, "{", "\\{");
	s = replaceAll(s, "}", "\\}");
	s = replaceAll(s, "_", "\\_");
	s = replaceAll(s, "%", "\\%");
	s = replaceAll(s, "$", "\\$");
	return s;
}

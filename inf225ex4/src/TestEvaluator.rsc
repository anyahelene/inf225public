module TestEvaluator
import Simple;
import Value;
import ParseTree;
import Environment;
import IO;
import Runner;

test bool Numbers() { return run((Program)`5`) == Int(5); }

test bool Arith1() { return run((Program)`5+5`) == Int(10); }
test bool Arith2() { return run((Program)`1+2*3`) == Int(7); }
test bool Arith3() { return run((Program)`10-2`) == Int(8); }
test bool Arith4() { return run((Program)`10/2`) == Int(5); }
test bool Arith5() { return run((Program)`(1+2)*3`) == Int(9); }
test bool Arith6() { return run((Program)`((10)/(2))`) == Int(5); }



test bool If1() {
	return run((Program)`if 2*3 == 6 then 42 else 69 end`) == Int(42);
}
test bool If2() {
	return run((Program)`if 6 == 5 then 42 else 69 end`) == Int(69);
}
test bool If3() {
	return run((Program)`if 10 \< 5 then 1 else 0 end`) == Int(0);
}


test bool Let1() {
	return run((Program)`let x = 2 in x end`) == Int(2);
}

test bool Let2() {
	run((Program)`fun f(int x) : int = x; let x = 2 in f(x) + x end`);
	return true;
}
test bool Let3() {
	run((Program)`fun f(int x) : int = x; let x = f in let x = 2 in x + 2 end end`);
	return true;
}

test bool LetFail1() {
	try {
		// undeclared variable
		run((Program)`let x = x in x end`);
	}
	catch str _: return true;
	return false;
}
test bool LetFail2() {
	try {
		// use variable after scope exit
		run((Program)`let x = 2 in x end; x`);
	}
	catch str _: return true;
	return false;
}


test bool Fun1() {
	return run((Program)`fun f(int x) : int = 5; f(f(f(2)))`) == Int(5);
}

test bool Fun2() {
	return run((Program)`fun f(int x) : int = 5; fun g() : int = f(2); g()`) 
			== Int(5);
}

test bool Fun3() {
	return run((Program)`fun fib(int n) : int = if n \< 2 then 1 else fib(n-1)+fib(n-2) end; fib(10)`) 
			== Int(89);
}

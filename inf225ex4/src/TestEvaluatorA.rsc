module TestEvaluator
import Simple;
import Value;
import ParseTree;
import Environment;
import IO;
import Runner;

test bool Numbers() { return run((Program)`int main() { return 5; }`) == Int(5); }

test bool Arith1() { return run((Program)`int main() { return 5+5; }`) == Int(10); }
test bool Arith2() { return run((Program)`int main() { return 1+2*3; }`) == Int(7); }
test bool Arith3() { return run((Program)`int main() { return 10-2; }`) == Int(8); }
test bool Arith4() { return run((Program)`int main() { return 10/2; }`) == Int(5); }
test bool Arith5() { return run((Program)`int main() { return (1+2)*3; }`) == Int(9); }
test bool Arith6() { return run((Program)`int main() { return ((10)/(2)); }`) == Int(5); }



test bool If1() {
	return run((Program)`int main() { if 2*3 == 6 then return 42; else return 69; end }`) == Int(42);
}
test bool If2() {
	return run((Program)`int main() { if 6 == 5 then return 42; else return 69; end }`) == Int(69);
}
test bool If3() {
	return run((Program)`int main() { if 10 \< 5 then return 1; else return 0; end }`) == Int(0);
}


test bool Let1() {
	return run((Program)`int main() { int x = 2; return x; }`) == Int(2);
}

test bool Let2() {
	return run((Program)`int f(int x) { return x; } int main() { int x = 2; return f(x) + x; }`) == Int(4);
	return true;
}
test bool Let3() {
	run((Program)`int f(int x) { return x; } int main() { int(int) x = f; if 1 then int x = 2; return x + 2; else return 2; end }`);
	return true;
}

test bool LetFail1() {
	try {
		// undeclared variable
		run((Program)`int main() { int x = x; return x; }`);
	}
	catch str _: return true;
	return false;
}
test bool LetFail2() {
	try {
		// use variable after scope exit
		run((Program)`int main() { { int x = 2; } return x; }`);
	}
	catch str _: return true;
	return false;
}


test bool Fun1() {
	return run((Program)`int f(int x) { return 5;} int main() { return f(f(f(2))); }`) == Int(5);
}

test bool Fun2() {
	return run((Program)`int f(int x) { return 5; } int g() { return f(2); } int main() { return g(); }`)
			== Int(5);
}

test bool Fun3() {
	return run((Program)`int fib(int n) { if n \< 2 then return 1; else return fib(n-1)+fib(n-2); end } int main() { return fib(10); }`) 
			== Int(89);
}

test bool Example1() {
	return run(parse(#start[Program], |project://inf225ex4/src/Example.A|).top) == Int(1);
}

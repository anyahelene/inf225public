module TestTypeChecker
import Simple;
import Value;
import TypeChecker;
import ParseTree;
import Environment;
import IO;

test bool Numbers() { return getType(checkExpr((Expr)`5`, newEnv(#Type))) == Int(); }

test bool Arith1() { return getType(checkExpr((Expr)`5+5`, newEnv(#Type))) == Int(); }
test bool Arith2() { return getType(checkExpr((Expr)`1+2*3`, newEnv(#Type))) == Int(); }
test bool Arith3() { return getType(checkExpr((Expr)`10-2`, newEnv(#Type))) == Int(); }
test bool Arith4() { return getType(checkExpr((Expr)`10/2`, newEnv(#Type))) == Int(); }
test bool Arith5() { return getType(checkExpr((Expr)`(1+2)*3`, newEnv(#Type))) == Int(); }
test bool Arith6() { return getType(checkExpr((Expr)`((10)/(2))`, newEnv(#Type))) == Int(); }



test bool If1() {
	check((Program)`int main() { if 2*3 == 6 then return 42; else return 69; end }`);
	return true;
}
test bool If2() {
	check((Program)`int main() { if 6 == 5 then return 42; else return 69; end }`);
	return true;
}
test bool If3() {
	check((Program)`int main() { if 10 \< 5 then return 1; else return 0; end }`);
	return true;
}

test bool IfFail1() {
	try {
		// condition should be int, not a function
		check((Program)`int f() { return 2; } int main() { if f then 1; else 2; end }`);
	}
	catch str _: return true;
	return false;
}

test bool Let1() {
	check((Program)`int main() { int x = 2; return x; }`);
	return true;
}
test bool Let2() {
	check((Program)`int f(int x) { return x; } int main() { int x = 2; return f(x) + x; }`);
	return true;
}
test bool Let3() {
	// shadowing variable
	check((Program)`int f(int x) { return x; } int main() { int(int) x = f; { int x = 2; return x + 2; } }`);
	return true;
}

test bool LetFail1() {
	try {
		// undeclared variable
		check((Program)`int main() { int x = x; return x; }`);
	}
	catch str _: return true;
	return false;
}
test bool LetFail2() {
	try {
		// use variable after scope exit
		check((Program)`int main() { { int x = 2; } return x; }`);
	}
	catch str _: return true;
	return false;
}

test bool LetFail3() {
	try {
		// redeclared variable
		check((Program)`int f(int x) { return x; } int main() { int(int) x = f; int x = 2; return x + 2; }`);
	}
	catch str _: return true;
	return false;
}

test bool AssignFail1() {
	try {
		// wrong type in assignment
		check((Program)`int f(int x) { return x; } int main() {  int(int) x = f; x = 2; }`);
	}
	catch str _: return true;
	return false;
}

test bool FailFun1() {
	try {
		// wrong return type
		check((Program)`int f(int x) { return 5; } int(int) g() { return 5; }`); 
	}
	catch str _: return true;
	return false;
}
test bool FailFun2() {
	try {
		// wrong return type
		check((Program)`int f(int x) { return 5; } int g() { return f; }`); 
	}
	catch str _: return true;
	return false;
}
test bool FailFun3() {
	try {
		// type error in body
		check((Program)`int f(int(int) x) { return x + 5; }`); 
	}
	catch str _: return true;
	return false;
}

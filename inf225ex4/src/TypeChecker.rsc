module TypeChecker
import Simple;
import String;
import ParseTree;
import IO;
import Environment;
import Value;
import Store;
import List;

data Type
	= Int()
	| Str()
	| Fun(list[Type] paramTypes, Type returnType)
	;
	

private Type evalType(Tree typ) {
	switch(typ) {
		case (SimpleTypeExpr)`int`: return Int();
		case (SimpleTypeExpr)`str`: return Str();
		case (SimpleTypeExpr)`(<TypeExpr t>)`: return evalType(t);
		case (TypeExpr)`<SimpleTypeExpr t>`: return evalType(t);
		case (TypeExpr)`<{SimpleTypeExpr ","}* pTypes> -\> <TypeExpr rType>`:
			return Fun([evalType(p) | p <- pTypes], evalType(rType));
		default: throw "Unknown type <typ>: <[typ]>";
	}
}


@doc{Declare a function. The function is added to the environement, but the body
is left unchecked.}
public Env[Type] define((Decl)`fun <ID f>(<{ParamDecl ","}* paramList>) : <TypeExpr retType> = <Expr body>;`, Env[Type] env) {
	list[Type] paramTypes = [];
	for((ParamDecl)`<TypeExpr pType> <ID pName>` <- paramList) {
		paramTypes += evalType(pType);
	}

	env = declare(f,  Fun(paramTypes, evalType(retType)), env);
	
	return env;
}

@doc{Check a function declaration. The function is assumed to be already added to the
environment (via define()), and we now check the body.}
public Decl check((Decl)`fun <ID f>(<{ParamDecl ","}* paramList>) : <TypeExpr retType> = <Expr body>;`, Env[Type] env) {
	list[Type] paramTypes = [];
	list[ID] paramNames = [];
	for((ParamDecl)`<TypeExpr pType> <ID pName>` <- paramList) {
		paramTypes += evalType(pType);
		paramNames += pName;
	}

	env = enterScope(env);
	for(<pType, pName> <- zip(paramTypes, paramNames)) {
		env = declare(pName, pType, env);
	}
	
	<body, actualRetType> = check(body, env);
	env = exitScope(env);
	
	if(actualRetType != evalType(retType)) {
		throw "Return type doesn\'t match";
	}
	
	return (Decl)`fun <ID f>(<{ParamDecl ","}* paramList>) : <TypeExpr retType> = <Expr body>;`;
}

@doc{Typecheck a program.}
public tuple[Program, Type] check((Program)`<Decl* decls><Expr e>`) {
	//try {
		Env[Type] env = newEnv(#Type);
		for(Decl d <- decls) {
			env = define(d, env);
		}
		
		// rewrite the parse tree of the declaration list
		// to include the results of typechecking
		decls = top-down-break visit(decls) {
			case Decl d => check(d, env)
		}
		
		<e, typ> = check(e, env);
		return <(Program)`<Decl* decls><Expr e>`, typ>;
	//}
	//catch ex: {
	//	println(ex);
	//	return (Program)`0`;
	//}
}

@doc{The type of an integer literal is int.}
public tuple[Expr, Type] check((Expr)`<NUM i>`, Env[Type] env) {
	return <(Expr)`<NUM i>`, Int()>;
}

@doc{The type of a variable is the type it's bound to in the environment.}
public tuple[Expr, Type] check((Expr)`<ID x>`, Env[Type] env) {
	typ = lookup(x, env);
	return <(Expr)`<ID x>`, typ>;
	
	// here, we could also change the name, if we wanted to, e.g., include the type in
	// the name:
	// return <parse(#Expr, "<x>_<mangleType(typ)>"), typ>;
}

@doc{Function calls.}
public tuple[Expr, Type] check((Expr)`<ID f>(<{Expr ","}* argList>)`, Env[Type] env) {
	funTyp = lookup(f, env);
	
	list[Type] argTypes = [];
	list[Expr] argChecked = [];
	
	for(a <- argList) { // type check arguments
		<a, aTyp> = check(a, env);
		argTypes += aTyp;
		argChecked += a;
	}
	argList = makeArgList(argChecked);
	
	if(Fun(paramTypes, returnTyp) := funTyp) {
		// check that actual argument types line up with formal parameter types
		for(<aTyp, pTyp> <- zip(argTypes, paramTypes)) {
			if(aTyp != pTyp)
				throw "Wrong argument type <aTyp> to function <f>, expected <pTyp>, at <expr@\loc>";
		}
		return <(Expr)`<ID f>(<ArgList argList>)`, returnTyp>;
	}
	else {
		throw "Variable <f> is not a function, at <f@\loc>";
	}
}

@doc{Parenthesis.}
public tuple[Expr, Type] check((Expr)`(<Expr e>)`, Env[Type] env) {
	<e, typ> = check(e, env);
	return <(Expr)`(<Expr e>)`, typ>;
}

@doc{Arithmetic. Check that both operands are ints.}
public tuple[Expr, Expr, Type] checkArithOp(Expr e1, Expr e2, Env[Type] env) {
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(<Int(), Int()> == <typ1, typ2>) {
		return <e1, e2, Int()>;
	}
	else {
		throw "Wrong operands \<<typ1>, <typ2>\> to operator";
	}
}

public tuple[Expr, Type] check((Expr)`<Expr e1>*<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>*<Expr e2>`, typ>;
}


public tuple[Expr, Type] check((Expr)`<Expr e1>+<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>+<Expr e2>`, typ>;
}


public tuple[Expr, Type] check((Expr)`<Expr e1>-<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>-<Expr e2>`, typ>;
}


public tuple[Expr, Type] check((Expr)`<Expr e1>/<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>/<Expr e2>`, typ>;
}

@doc{Helper for checking comparisons. We assume that things of the same type can be compared.
Always returns an int.}
public tuple[Expr, Expr, Type] checkCompOp(Expr e1, Expr e2, Env[Type] env) {
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(typ1 == typ2) {
		return <e1, e2, Int()>;
	}
	else {
		throw "Can\'t compare <typ1> and <typ2>, at <expr@\loc>";
	}
}


public tuple[Expr, Type] check((Expr)`<Expr e1> == <Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>==<Expr e2>`, typ>;
}

public tuple[Expr, Type] check((Expr)`<Expr e1>\<<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\<<Expr e2>`, typ>;
}

public tuple[Expr, Type] check((Expr)`<Expr e1>\><Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\><Expr e2>`, typ>;
}

public tuple[Expr, Type] check((Expr)`<Expr e1>\<=<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\<=<Expr e2>`, typ>;
}

public tuple[Expr, Type] check((Expr)`<Expr e1>\>=<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\>=<Expr e2>`, typ>;
}

@doc{'If' expression. Condition should be an int; type of branches must be the same.}
public tuple[Expr, Type] check((Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, Env[Type] env) {
	<c, cTyp> = check(c, env);
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(cTyp != Int()) { 
		throw "Condition must be an integer";
	}
	
	
	if(typ2 != typ1) {
		throw "If branches must have same type";
	}
	
	return <(Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, typ1>;
}


@doc{'Let' expression. Check the first expression, this gives the type of the variable. Then
bind the variable, and check the second expression.}
public tuple[Expr, Type] check((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env[Type] env) {
	<e1, typ1> = check(e1, env);

	env = enterScope(env);
	env = declare(x,  typ1, env);
	
	<e2, typ2> = check(e2, env);
	env = exitScope(env);
	// x = parse(#Expr, "<x>_<mangleType(typ1)>");

	return <(Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, typ2>;
}

@doc{Assignment. Variable type must match the expression type}
public tuple[Expr, Type] check((Expr)`<ID x>=<Expr e>`, Env[Type] env) {
	xTyp = lookup(x, env);
	<e, eTyp> = check(e, env);
	
	if(xTyp != eTyp) {
		throw "Wrong type in assignment";
	}
	
	return <(Expr)`<ID x>=<Expr e>`, xTyp>;	
}

@doc{Sequencing. Type of the whole sequence expression is type of the second expression.}
public tuple[Expr, Type] check((Expr)`<Expr e1>;<Expr e2>`, Env[Type] env) {
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	return <(Expr)`<Expr e1>;<Expr e2>`, typ2>;
}

// this will convert a type into a string suitable for
// including in an identifier name
private str mangleType(Type t) {
	switch(t) {
		case Int(): return "int";
		case Str():	return "str";
		case Fun(list[Type] ps, Type rt):
		return "<intercalate("_", [mangleType(p) | p <- ps])>__<mangleType(rt)>";
	}
}

@doc{Pick the type from a check() return tuple.}
public Type getType(tuple[&_, Type] tup) {
	return tup[1];
}


@doc{Pick the type from a check() return tuple.}
public Type getType(tuple[&_, Type, &_] tup) {
	return tup[1];
}


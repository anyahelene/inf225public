module PartEval
import Simple;
import String;
import ParseTree;
import IO;

public Program partEval(Program p) {
	return bottom-up visit(p) {
		case e:(Expr)`<NUM e1>+<NUM e2>`: {
			println(e);
			insert parseInt(toInt(unparse(e1)) + toInt(unparse(e2)));
		}
		case e:(Expr)`<NUM e1>*<NUM e2>`
			=> parseInt(toInt(unparse(e1)) * toInt(unparse(e2)))
	};
}

/* this won't work
public Expr partEval(Expr e, Env env) {
	return top-down-break visit(e) {
		case (Expr)`let <ID x> = <Expr e1> in <Expr e2> end`
			=> partEval(e2, declare(x, partEval(e1), env))
	};
}
*/

private Expr parseInt(int i) {
	return parse(#Expr, "<i>");
}
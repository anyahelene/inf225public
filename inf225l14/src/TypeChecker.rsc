module TypeChecker
import Simple;
import String;
import ParseTree;
import IO;
import Environment;
import Value;
import Store;
import List;

data DataType
	= Int()
	| Str()
	| Fun(list[DataType] paramTypes, DataType returnType)
	;
	

private DataType convert(Type typ) {
	switch(typ) {
		case (Type)`int`: return Int();
		case (Type)`str`: return Str();
		case (Type)`<{Type ","}* pTypes> -\> <Type rType>`:
			return Fun([convert(p) | p <- pTypes], convert(rType));
	}
}

@doc{Evaluate a function declaration. The declaration, and the (as yet
unevaluated) body is added to the environment, together with the environment
in which the function was defined (for lexical scoping).}
public Env[DataType] define((Decl)`fun <ID f>(<{ParamDecl ","}* paramList>) : <Type retType> = <Expr body>;`, Env[DataType] env) {
	list[DataType] paramTypes = [];
	for((ParamDecl)`<Type pType> <ID pName>` <- paramList) {
		paramTypes += convert(pType);
	}

	env = declare(f,  Fun(paramTypes, convert(retType)), env);
	
	return env;
}

@doc{Evaluate a function declaration. The declaration, and the (as yet
unevaluated) body is added to the environment, together with the environment
in which the function was defined (for lexical scoping).}
public void check((Decl)`fun <ID f>(<{ParamDecl ","}* paramList>) : <Type retType> = <Expr body>;`, Env[DataType] env) {
	list[DataType] paramTypes = [];
	list[ID] paramNames = [];
	for((ParamDecl)`<Type pType> <ID pName>` <- paramList) {
		paramTypes += convert(pType);
		paramNames += pName;
	}

	for(<pType, pName> <- zip(paramTypes, paramNames)) {
		env = declare(pName, pType, env);
	}
	
	<body, actualRetType> = check(body, env);
	
	if(actualRetType != convert(retType)) {
		throw "Return type doesn\'t match";
	}
}

@doc{Evaluate a program.}
public Program check((Program)`<Decl* decls><Expr e>`) {
	//try {
		Env[DataType] env = newEnv(#DataType);
		for(Decl d <- decls) {
			env = define(d, env);
		}
			
		for(Decl d <- decls) {
			check(d, env);
		}
		<e, typ> = check(e, env);
		return e;
	//}
	//catch ex: {
	//	println(ex);
	//	return (Program)`0`;
	//}
}

@doc{The value of an integer literal is the value of integer itself.}
public tuple[Expr, DataType] check(expr:(Expr)`<NUM i>`, Env[DataType] env) {
	return <expr, Int()>;
}

@doc{The value of a variable is the value it's bound to in the environment.}
public tuple[Expr, DataType] check(expr:(Expr)`<ID x>`, Env[DataType] env) {
	typ = lookup(x, env);
	return <expr, typ>;
}

@doc{Function calls. Find the function definition first; evaluate the
argument, then evaluate the function body in the function's definition
environment, with the argument is bound to the value.}
public tuple[Expr, DataType] check(expr:(Expr)`<ID f>(<{Expr ","}* argList>)`, Env[DataType] env) {
	funTyp = lookup(f, env);
	
	list[DataType] argTypes;
	
	argTypes = for(a <- argList) {
		<a, aTyp> = check(a, env);
		append aTyp;
	}
	
	if(Fun(paramTypes, returnTyp) := funTyp) {
		for(<aTyp, pTyp> <- zip(argTypes, paramTypes)) {
			if(aTyp != pTyp)
				throw "Wrong argument type <aTyp> to function <f>, expected <pTyp>, at <expr@\loc>";
		}
		return <expr, returnTyp>;
	}
	else {
		throw "Variable <f> is not a function, at <f@\loc>";
	}
}

@doc{Parenthesis.}
public tuple[Expr, DataType] check((Expr)`(<Expr e>)`, Env[DataType] env) {
	return check(e, env);
}

public tuple[Expr, Expr, DataType] checkArithOp(Expr e1, Expr e2, Env[DataType] env) {
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(<Int(), Int()> == <typ1, typ2>) {
		return <e1, e2, Int()>;
	}
	else {
		throw "Wrong operands \<<typ1>, <typ2>\> to operator, at <expr@\loc>";
	}
}
@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Expr, DataType] check((Expr)`<Expr e1>*<Expr e2>`, Env[DataType] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>*<Expr e2>`, typ>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Expr, DataType] check((Expr)`<Expr e1>+<Expr e2>`, Env[DataType] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>+<Expr e2>`, typ>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Expr, DataType] check((Expr)`<Expr e1>-<Expr e2>`, Env[DataType] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>-<Expr e2>`, typ>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Expr, DataType] check((Expr)`<Expr e1>/<Expr e2>`, Env[DataType] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>/<Expr e2>`, typ>;
}

public tuple[Expr, Expr, DataType] checkCompOp(Expr e1, Expr e2, Env[DataType] env) {
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(typ1 == typ2) {
		return <e1, e2, Int()>;
	}
	else {
		throw "Can\'t compare <typ1> and <typ2>, at <expr@\loc>";
	}
}


@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Expr, DataType] check((Expr)`<Expr e1> == <Expr e2>`, Env[DataType] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>==<Expr e2>`, typ>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Expr, DataType] check((Expr)`<Expr e1>\<<Expr e2>`, Env[DataType] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\<<Expr e2>`, typ>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Expr, DataType] check((Expr)`<Expr e1>\><Expr e2>`, Env[DataType] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\><Expr e2>`, typ>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Expr, DataType] check((Expr)`<Expr e1>\<=<Expr e2>`, Env[DataType] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\<=<Expr e2>`, typ>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Expr, DataType] check((Expr)`<Expr e1>\>=<Expr e2>`, Env[DataType] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\>=<Expr e2>`, typ>;
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public tuple[Expr, DataType] check(expr:(Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, Env[DataType] env) {
	<c, cTyp> = check(c, env);
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(c != Int()) { 
		throw "Condition must be an integer";
	}
	
	
	if(typ2 != typ1) {
		throw "If branches must have same type";
	}
	
	return <expr, typ1>;
}


@doc{'Let' expression. Evaluate the first expression, then evaluate the
second in an environment where the variable is bound to the result of the
first expression.}
public tuple[Expr, DataType] check(expr:(Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env[DataType] env) {
	<_, typ1> = check(e1, env);

	env = enterScope(env);
	env = declare(x,  typ1, env);
	
	<_, typ2> = check(e2, env);

	return <expr, typ2>;
}

public tuple[Expr, DataType] check(expr:(Expr)`<ID x>=<Expr e>`, Env[DataType] env) {
	xTyp = lookup(x, env);
	<e, eTyp> = check(e, env);
	
	if(xTyp != eTyp) {
		throw "Wrong type in assignment";
	}
	
	return <expr, xTyp>;	
}

public tuple[Expr, DataType] check(expr:(Expr)`<Expr e1>;<Expr e2>`, Env[DataType] env) {
	<_, typ1> = check(e1, env);
	<_, typ2> = check(e2, env);
	
	return <expr, typ2>;
}


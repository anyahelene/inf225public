module Value
import Simple;
import Environment;

@doc{Values. Integers and functions are values}
data Value
	= Int(int intValue)
	| Fun(list[str] argNames, Expr expr)
	;

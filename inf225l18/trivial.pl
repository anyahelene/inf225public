:- op(901, yfx, (=>)).

expr(X) => V :- expr(X, []) => V.

expr(int(X), _Env) => intval(X).

expr(str(X), _Env) => strval(X).

expr(var(X), Env) => V :-
	lookup(X,V,Env).

expr(X + Y, Env) => intval(V) :-
       expr(X, Env) => intval(Vx),
       expr(Y, Env) => intval(Vy),
       V is Vx+Vy.

%
% Attempt at small step rules
%
% expr(intval(X) + intval(Y), _Env) => V :-
%	V is X+Y.

% expr(X + Y, _Env) => intval(Vx) + Y :-
%	expr(X, Env) => intval(Vx).

% expr(X + Y, _Env) => X + intval(Vy) :-
%	expr(Y, Env) => intval(Vy).

expr(ifThenElse(C,T,_E), Env) => V :-
	expr(C, Env) => 1,
	expr(T, Env) => V.

expr(ifThenElse(C,_T,E), Env) => V :-
	expr(C, Env) => 0,
	expr(E, Env) => V.

expr(let(var(X),E1,E2), Env) => V2 :-
	expr(E1, Env) => V1,
	expr(E2, bind(X,V1,Env)) => V2.


lookup(X,V,bind(X,V,_Env)) :- !.

lookup(X,V,bind(_X,_V,Env)) :-
	lookup(X,V,Env).

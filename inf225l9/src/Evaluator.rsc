module Evaluator
import Simple;
import String;
import ParseTree;
import IO;

alias Env = map[str, Value];

data Value
	= Int(int intValue)
	| Fun(str argName, Expr expr, map[str, Value] env)
	;


public void printEnv(Env env) {
	for(k <- env) {public Value eval((Expr)`<Expr e1>==<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue == eval(e2, env).intValue ? 1 : 0);
}
	
		if(Fun(argName, expr, fenv) := env[k])
			println("  <k>: Fun(<argName>, <unparse(expr)>, <fenv>)");
		else
			println("  <k>: <env[k]>");
	}
}

public Env define((Decl)`fun <ID f>(<ID arg>) = <Expr body>;`, Env env) {
	envNew = env + (unparse(f) : Fun(unparse(arg), body, env));
	println("Defining function <f>(<arg>) = <body>;");
	printEnv(env);
	// env[unparse(f)] = Fun(unparse(arg), body, env);
	return envNew;
}

public Value eval((Program)`<Decl* decls><Expr e>`) {
	Env env = ();
	for(Decl d <- decls) {
		env = define(d, env);
	}
		
	return eval(e, env);
}

public Value eval((Expr)`<NUM i>`, Env env) {
	return Int(toInt(unparse(i)));
}

public Value eval((Expr)`<ID x>`, Env env) {
	return env[unparse(x)];
}

public Value eval((Expr)`<ID f>(<Expr e>)`, Env env) {
	fun = env[unparse(f)];	
	arg = eval(e, env);
	
	if(Fun(argName, body, envIn) := fun) {
		Env funEnv = envIn;
//		if(funEnv == ())
//			funEnv = (argName : arg);
//		else
			funEnv = funEnv + (unparse(f) : Fun(argName, body, funEnv));
			funEnv = funEnv + (argName : arg);
		return eval(body, funEnv);
	}
	else {
		throw "Expression should be a function";
	}
}

public Value eval((Expr)`<Expr e1>*<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue * eval(e2, env).intValue);
}

public Value eval((Expr)`<Expr e1>+<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue + eval(e2, env).intValue);
}

public Value eval((Expr)`<Expr e1>-<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue - eval(e2, env).intValue);
}

public Value eval((Expr)`<Expr e1>/<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue / eval(e2, env).intValue);
}

public Value eval((Expr)`<Expr e1> == <Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue == eval(e2, env).intValue ? 1 : 0);
}

public Value eval((Expr)`<Expr e1>\<<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue < eval(e2, env).intValue ? 1 : 0);
}

public Value eval((Expr)`<Expr e1>\><Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue > eval(e2, env).intValue ? 1 : 0);
}

public Value eval((Expr)`<Expr e1>\<=<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue <= eval(e2, env).intValue ? 1 : 0);
}

public Value eval((Expr)`<Expr e1>\>=<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue >= eval(e2, env).intValue ? 1 : 0);
}

public Value eval((Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, Env env) {
	if(eval(c, env).intValue != 0)
		return eval(e1, env);
	else
		return eval(e2, env);
}


public Value eval((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env env) {
	v = eval(e1, env);
	
	innerEnv = env + ([unparse(x)] : v);
	
	return eval(e2, innerEnv);
}

